package be.swl.formation.spring.controller;

import be.swl.formation.spring.model.Forum;
import be.swl.formation.spring.model.User;
import be.swl.formation.spring.repository.forum.ForumService;
import be.swl.formation.spring.repository.forum.ForumServiceImpl;
import be.swl.formation.spring.repository.user.UserService;
import be.swl.formation.spring.repository.user.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.util.List;

@Controller
public class HomeController {
    private UserService userService;
    private ForumService forumService;

    @Autowired
    public HomeController(UserServiceImpl userService, ForumServiceImpl forumService) {
        this.userService = userService;
        this.forumService = forumService;
    }

    @GetMapping(path = {"", "/", "/home"})
    public String IndexAction(Model model) {
        List<User> users = this.userService.getAll();
        model.addAttribute("users", users);
        return "home/index";
    }

    @GetMapping(path = {"/forum/deleted"})
    public String ForumAction(Model model) {
        List<Forum> fora = this.forumService.getAll(true);
        model.addAttribute("fora", fora);

        return "home/forum/deleted";
    }
}
