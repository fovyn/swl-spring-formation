package be.swl.formation.spring.repository.user;

import be.swl.formation.spring.model.User;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public interface UserService {
    User getOne(@NotNull() Long id);
    List<User> getAll();
    List<User> getAllByName(String name) throws Exception;
}
