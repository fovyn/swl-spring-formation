package be.swl.formation.spring.exemple;

public interface IObservable {
    void subscribe(IObserver observer);
}
