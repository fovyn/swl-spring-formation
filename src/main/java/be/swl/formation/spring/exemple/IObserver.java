package be.swl.formation.spring.exemple;

public interface IObserver {
    void notify(double lp);
}
