package be.swl.formation.spring.repository.user;

import be.swl.formation.spring.model.User;
import be.swl.formation.spring.repository.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class UserServiceImpl extends BaseServiceImpl<UserRepository, User> implements UserService {

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        super(repository);
    }

    @Override
    public User getOne(@NotNull() Long id) {
        return this.repository().getOne(id);
    }

    @Override
    public List<User> getAll() {
        return this.repository().findAll();
    }

    @Override
    public List<User> getAllByName(String name) throws Exception {
        if(name.equals("Admin"))throw new Exception();
        return this.repository().findAllByName(name);
    }
}
