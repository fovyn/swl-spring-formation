package be.swl.formation.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public abstract class BaseServiceImpl<I extends JpaRepository<C, Long>,C> {
    private I _repository;
    public I repository() {
        return _repository;
    }

    public BaseServiceImpl(I repository) {
        this._repository = repository;
    }
}
