package be.swl.formation.spring.exemple;

import java.util.Set;

public class WorldBoss implements IObservable {
    private Set<IObserver> players;

    @Override
    public void subscribe(IObserver observer) {
        this.players.add(observer);
    }
}
