package be.swl.formation.spring.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "Swl_Message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id")
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate createAt;

    @Column
    private LocalDate updateAt;

    @Column
    private boolean isAllowed;

    @ManyToOne(targetEntity = Subject.class)
    @JoinColumn(name = "subject_id")
    private Subject subject;
}
