package be.swl.formation.spring.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "category_id")
    private Long id;

    @Column
    private String name;

    @ManyToMany(targetEntity = Forum.class, mappedBy = "categories")
    private List<Forum> fora;
}
