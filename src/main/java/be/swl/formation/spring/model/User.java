package be.swl.formation.spring.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.MoreObjects;
import com.google.common.collect.Maps;
import com.google.common.collect.Tables;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "SWL_User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(unique = true, length = 82)
    private String username;
    @Column
    private String password;
    @Column
    private LocalDate lastConnect;

    @OneToMany(targetEntity = UserRight.class, mappedBy = "user")
    @OrderColumn
    private UserRight[] userRights;

//    @Column(name = "group_id")
//    private Long groupId;

    @ManyToOne(targetEntity = Group.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id")
    private Group group;

    //region Constructors
    public User() {
    }
    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
    //endregion

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getLastConnect() {
        return lastConnect;
    }
    public void setLastConnect(LocalDate lastConnect) {
        this.lastConnect = lastConnect;
    }

    public UserRight[] getUserRights() {
        return userRights;
    }

    public void setUserRights(UserRight[] userRights) {
        this.userRights = userRights;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("username", username)
                .add("password", password)
                .add("lastConnect", lastConnect)
                .toString();
    }
}
