package be.swl.formation.spring.repository.forum;

import be.swl.formation.spring.model.Forum;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ForumService {
    List<Forum> getAll(boolean isDeleted);
}
