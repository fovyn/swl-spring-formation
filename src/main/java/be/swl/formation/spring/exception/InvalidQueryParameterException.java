package be.swl.formation.spring.exception;

public class InvalidQueryParameterException extends Exception {
    public InvalidQueryParameterException() {
    }

    public InvalidQueryParameterException(String message) {
        super(message);
    }
}
