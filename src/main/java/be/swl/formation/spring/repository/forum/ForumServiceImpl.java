package be.swl.formation.spring.repository.forum;

import be.swl.formation.spring.model.Forum;
import be.swl.formation.spring.repository.BaseServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ForumServiceImpl extends BaseServiceImpl<ForumRepository, Forum> implements ForumService {
    public ForumServiceImpl(ForumRepository repository) {
        super(repository);
    }


    @Override
    public List<Forum> getAll(boolean isDeleted) {
        return this.repository().findAll(isDeleted);
    }
}
