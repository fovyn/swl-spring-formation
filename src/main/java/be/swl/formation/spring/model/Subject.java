package be.swl.formation.spring.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "Swl_Subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "subject_id")
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate createAt;

    @Column
    private LocalDate updateAt;

    @Column
    private boolean isAllowed;

    @ManyToOne(targetEntity = Forum.class)
    @JoinColumn(name = "forum_id")
    private Forum forum;

    @ManyToMany(targetEntity = User.class)
    @JoinTable(
            name = "Swl_SubjectModerators",
            joinColumns = @JoinColumn(name = "subject_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private List<User> moderators;

    @OneToMany(targetEntity = Subject.class, mappedBy = "parent")
    private List<Subject> children;

    @ManyToOne(targetEntity = Subject.class, optional = true)
    @JoinColumn(name = "parent_id")
    private Subject parent;

    @OneToMany(targetEntity = Message.class, mappedBy = "subject")
    private List<Message> messages;
}
