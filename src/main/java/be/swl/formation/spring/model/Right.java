package be.swl.formation.spring.model;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Swl_Right")
public class Right {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Right_Id")
    private Long id;

    @Column(name = "Right_Name")
    private String name;

    @ManyToMany(targetEntity = Group.class, mappedBy = "rights")
    @OrderColumn
    private Group[] groups;

    public Right() {
    }

    public Right(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Right)) return false;
        Right right = (Right) o;
        return Objects.equals(id, right.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
