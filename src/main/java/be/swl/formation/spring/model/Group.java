package be.swl.formation.spring.model;

import com.google.common.base.MoreObjects;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "Swl_Group")
public class Group {
    //region Attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Group_Id")
    private Long id;

    @Column(name = "Group_Name")
    private String name;

    @OneToMany(targetEntity = User.class, mappedBy = "group")
    @OrderColumn
    private User[] users;

    @ManyToMany(targetEntity = Right.class)
    @JoinTable(
            name = "Swl_GroupRights",
            joinColumns = @JoinColumn(name = "group_id"),
            inverseJoinColumns = @JoinColumn(name = "right_id")
    )
    @OrderColumn
    private Right[] rights;
    //endregion
    //region Constructors
    public Group() {
    }
    public Group(String name) {
        this.name = name;
    }
    //endregion
    //region Getters and Setters
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public Right[] getRights() {
        return rights;
    }

    public void setRights(Right[] rights) {
        this.rights = rights;
    }

    //endregion
    //region Methods

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Group)) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id) &&
                Objects.equals(name, group.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
//endregion
}
