package be.swl.formation.spring.repository.forum;

import be.swl.formation.spring.model.Forum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ForumRepository extends JpaRepository<Forum, Long> {

    @Query("SELECT f FROM Forum f WHERE f.deleted = :isDeletedParam")
    List<Forum> findAll(@Param("isDeletedParam") boolean isDeleted);
}
